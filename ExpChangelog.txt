General Balance Changes

- Experience awards of (mini-hero) units no longer increase when levels are gained. Note: this does not include heroes.
- Experience requirement for (mini-hero) unit leveling up now increases progressively (was linearly). Note: this does not include heroes.
- Health and damage bonus per level is now +5% (of base stats) for heroes and mini-hero hordes.
- Health and damage bonus per level is now +10% (of base stats) for regular units.
- Heroes' experience award corrected.
--- Tier 1 hero experience award increased to 55..79 (from 15..75 -> lowest level..highest level)
--- Tier 2 hero experience award increased to 70..101 (from 20..100)
--- Tier 3 hero experience award changed to 85..123 (from 30..125)
--- Tier 4 hero experience award increased to 100..145 (from 35..145)
--- Tier 5 hero experience award changed to 120..174 (from 40..175)
- Weak production buildings (such as orc pits):
--- Level 1 award reduced to 40 (from 50)
--- Level 2 award reduced to 50 (from 60)
--- Level 3 award reduced to 60 (from 70)
- Siege equipment:
--- Weak siege award (e.g. siege ladders and rams) increased to 20 (from 10).
--- Strong siege award (demolisher and catapults) increased to 30 (from 10).

Men

- Knights of Dol Amroth
--- Horde experience award increased to 100 (from 40..85 -> lowest level..highest level)
- Tower guards
--- Horde experience award decreased to 60 (from 75..135)

Elves

- Fortress Eagle
--- Experience award increased to 85 (from 8..17)
- Noldor warriors
--- Horde experience award increased to 102 (from 48..102)
- Mirkwood archers
--- Horde experience award changed to 60 (from 50..90)
- Lorien warriors
--- Horde experience award changed to 48 (from 36..84)
- Lorien archers
--- Horde experience award changed to 48 (from 36..84)

Dwarves

- Zealots
--- Horde experience award increased to 80 (from 32..68)
- Axe throwers
--- Horde experience award changed to 48 (from 36..84)
- Guardians
--- Horde experience award changed to 40 (from 30..80)
- Battle Wagon
--- Mounted axe throwers, men of dale and phalanx now earn power points when dealing damage
--- Earn experience when mounted units kill enemies or destroy structures
--- Experience award increased to 80 (from 10..30)

Isengard

- Deathbringers
--- Horde experience award increased to 78 (from 24..51)
- Berserker
--- Experience award increased to 20 (from 5..9)
- Warg pack
--- Horde experience award changed to 40 (from 24..56)
- Wildmen
--- Horde experience award changed to 24 (from 12..60)
- Wildmen axe throwers
--- Horde experience award changed to 36 (from 12..60)

Mordor

- Attack troll
--- Experience award increased to 80 (from 30..70)
- Mumak
--- Experience award changed to 80 (from 50..90)
- Black riders
--- Horde experience award increased to 120 (from 48..102)
- Drummer troll
--- Experience award changed to 45 (from 30..70)
- Mountain troll
--- Experience award changed to 45 (from 30..70)
- Black orcs
--- Horde experience award changed to 48 (from 36..84)
- Orc archers
--- Horde experience award changed to 20 (from 20..100)
- Easterlings
--- Horde experience award decreased to 45 (from 75..135)

Goblins

- Fortress Fire drake
--- Experience award increased to 85 (from 8..17)
- Fire drake brood
--- Horde experience award increased to 99 (from 24..51)
- Spider riders
--- Horde experience award increased to 78 (from 48..72)
- Mountain giant
--- Experience award changed to 60 (from 30..70)
- Cave troll
--- Experience award changed to 45 (from 30..70)
- Spiderlings
--- Horde experience award changed to 40 (from 30..70)
- Half-troll marauders
--- Horde experience award changed to 48 (from 40..72)
- Half-troll swordsmen
--- Horde experience award changed to 48 (from 40..72)
- Goblin archers
--- Horde experience award changed to 20 (from 20..100)

Angmar

- Snow trolls
--- Horde experience award increased to 78 (from 48..72)
- Gundabad warriors
--- Horde experience award changed to 25 (from 21..105)
- Rhudaur spearmen
--- Horde experience award changed to 25 (from 16..80)
- Rhudaur axe throwers
--- Horde experience award changed to 25 (from 11..55)
- Sorcerer
--- Horde experience award increased to 45 (from 12..44)
- Direwolf pack
--- Horde experience award changed to 40 (from 24..56)
- Hill trolls
--- Horde experience award changed to 60 (from 50..90)
- Wolf riders
--- Horde experience award decreased to 66 (from 88..132)
