/*
 * main.cpp
 *
 * Copyright 2017 Bram Bosch <bagbosch@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

// For GUI
#include <QApplication>
#include <QWidget>
#include <QDebug>

#include <QSettings>
#include <QDesktopWidget>

// For file handling
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QVector>
#include <QSettings>

// For md5sum
#include <QCryptographicHash>



/**
 * Calculates an sha1 checksum from a file.
 *
 * Will return the string "Error" if something went wrong
 *
 * Mapping:  QString -> QString
 */
QString fileSHA1(QString file_name)
{
	const char * output = "Error";

	QFile f(file_name);
	QCryptographicHash hash(QCryptographicHash::Sha1);

	if (f.open(QFile::ReadOnly))
	{
		hash.addData(f.readAll());

		output = hash.result().toHex();
	}

	return QString(output);
}



int main(int argc, char *argv[])
{
	QString directory;
	if(argc < 2)
		return 0;
	else
		directory = QString(argv[1]);


	QSettings settings(directory, QSettings::IniFormat);
	
	QVector<QVariant> retrievedList;
	
	int size = settings.beginReadArray("disableFiles");
	settings.endArray();

	settings.beginWriteArray("disableFiles");


	settings.setArrayIndex(size);
	settings.setValue("name", "#########202_v7.0.0.big");

	settings.setArrayIndex(size+1);
	settings.setValue("name", "lang/englishstrings202_v7.0.0.big");

	settings.sync();
	settings.endArray();

	return 0;
}
